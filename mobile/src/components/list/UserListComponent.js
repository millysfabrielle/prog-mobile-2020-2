import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, Button } from "react-native";
import { useNavigation } from '@react-navigation/native';

const Item = ( {item} ) => {

    const navigation = useNavigation();
    const btnDelete = <Button onPress={() => alert("User ID = ", item.id)} title="Deletar" />

    return(
        <View style={styles.wrapper}>
            <TouchableOpacity 
                key={""+item.id} 
                style={styles.container} 
                onPress={ () => navigation.navigate('UserDetails', { userID: item.id, title: "Teste", btnDelete: btnDelete  }) }>

                <Image style={styles.userLogo} source={require("../../../assets/user-avatar.png")} />
                <Text >{item.nome}</Text>
            </TouchableOpacity>
        </View>
    );

};


const UserListComponent = ({item}) => {
    
    return(
        <Item item={item} />
    );
}

export default UserListComponent;

const styles = StyleSheet.create({
    wrapper: {
        borderTopWidth: 1,
        borderTopColor: "#444",
        padding: 5
    },
    container:{
        flex: 1,
        flexDirection: 'row',
    },
    userLogo: {
      width: 50,
      height: 50,
      marginRight: 15,
      resizeMode: 'stretch',
    },
  });