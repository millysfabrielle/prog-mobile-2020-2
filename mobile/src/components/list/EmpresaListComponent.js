import React from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, Button } from "react-native";
import { useNavigation } from '@react-navigation/native';
import { block } from 'react-native-reanimated';

const Item = ( {item} ) => {

    const navigation = useNavigation();

    return(
        <View style={styles.wrapper}>
            <TouchableOpacity 
                key={""+item.id} 
                style={styles.container} 
                onPress={ () => navigation.navigate('EmpresaDetails', { id: item.id}) }>

                <Image style={styles.userLogo} source={require("../../../assets/user-avatar.png")} />
                <Text style={styles.text} >{item.nome}</Text>
                <Text style={styles.text} >{item.cnpj}</Text>
            </TouchableOpacity>
        </View>
    );

};


const EmpresaListComponent = ({item}) => {
    
    return(
        <Item item={item} />
    );
}

export default EmpresaListComponent;

const styles = StyleSheet.create({
    wrapper: {
        borderTopWidth: 1,
        borderTopColor: "#444",
        padding: 5
    },
    container:{
        flex: 1,
        flexDirection: 'row',
    },
    userLogo: {
      width: 50,
      height: 50,
      marginRight: 15,
      resizeMode: 'stretch',
    },
    text: {  

        width: "100%"
    }
  });