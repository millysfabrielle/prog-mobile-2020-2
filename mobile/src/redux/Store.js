// npm install react-redux
// npm install redux
// npm install redux-thunk

import {combineReducers, createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';


import pessoaReducer from './reducers/PessoaReducers';
import empresaReducer from "./reducers/EmpresaReducers";

// combinar todos os reducers do projeto
const reducers = combineReducers({
    pessoaState: pessoaReducer,
    empresaState: empresaReducer
});

// recuperar o Store da LocalStorage    condição ? true : false  (IF TERNÁRIO)


/* const localState = localStorage.getItem('applicationState')
? JSON.parse(localStorage.getItem('applicationState'))
: {}; */
const localState = {};

 const store = createStore(
    reducers,
    localState,
    applyMiddleware(thunk)
 );

/* store.subscribe( function () {
    localStorage.setItem('applicationState', JSON.stringify(store.getState()));
}) */;

export default store;