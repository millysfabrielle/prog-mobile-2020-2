import { useImperativeHandle } from "react";
import PessoaService from "../../services/PessoaService";

export const PESSOA_ACTIONS = {
    LISTAR: "PESSOA_LISTAR",
    BUSCAR: "PESSOA_BUSCAR",
    SALVAR: "PESSOA_SALVAR",
    ALTERAR: "PESSOA_ALTERAR",
    EXCLUIR: "PESSOA_EXCLUIR"
}

export function alterarPessoa(pessoa){
    return function(dispatch){
        return PessoaService.salvar(pessoa)
        .then( response => {
            dispatch({
                type: PESSOA_ACTIONS.ALTERAR,
                content: response.data
            })
        })
        .catch(error => {
            console.log(error);
        })
    }
}

export function salvarPessoa(pessoa){
    return function(dispatch){
        return PessoaService.salvar(pessoa)
        .then( response => {
            dispatch({
                type: PESSOA_ACTIONS.SALVAR,
                content: response.data
            })
        })
        .catch(error => {
            console.log(error);
        })
    }
}

export function deletarPessoa(id){
    return function(dispatch){

        return PessoaService.excluir(id)
        .then( response => {

            dispatch({
                type: PESSOA_ACTIONS.EXCLUIR,
                content: id
            })

        })
        .catch(error => {
            console.log(error);
        })
        
    }
}

export function buscarPessoa(id){
    return function(dispatch){
        return PessoaService.buscar(id)
        .then( response => {
            dispatch({
                type: PESSOA_ACTIONS.BUSCAR,
                content: response.data
            });
        } )
        .catch(error => {
            console.log(error);
        })
    }
}

export function listarPessoa(){
    return function (dispatch){

        return PessoaService.listar()       
        .then( response => {   //(parâmetros) => {implementação}

            dispatch({
                type: PESSOA_ACTIONS.LISTAR,
                content: response.data
            })

        })
        .catch(error => {
            console.log(error);
        })

    }
}