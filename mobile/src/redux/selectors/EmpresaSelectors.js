import values from "lodash/values";

export const empresaListaSelector = ( {empresaState} ) => {
     return { empresaLista: values(empresaState.empresaLista) }
};

export const empresaItemSelector = ( {empresaState} ) => {
    return { empresaItem: empresaState.empresaItem }
};