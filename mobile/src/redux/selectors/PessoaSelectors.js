import values from "lodash/values";

export const pessoaListaSelector = ( {pessoaState} ) => {
     return { pessoaLista: values(pessoaState.pessoaLista) }
};

export const pessoaItemSelector = ( {pessoaState} ) => {
    return { pessoaItem: pessoaState.pessoaItem }
};