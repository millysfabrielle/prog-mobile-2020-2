import {EMPRESA_ACTIONS} from "../actions/EmpresaActions";

const empresaState = {
    empresaLista: [],
    empresaItem: {}
}

export default function empresaReducer( state = empresaState, dispatch ){

    switch(dispatch.type){

        case EMPRESA_ACTIONS.LISTAR:
            return {
                ...state, /* SPREAD */
                empresaLista: dispatch.content
            }

        case EMPRESA_ACTIONS.BUSCAR:
            return {
                ...state,
                empresaItem: dispatch.content
            }

        case EMPRESA_ACTIONS.EXCLUIR:
            return {
                ...state,
                empresaLista: state.empresaLista.filter(  function(item){
                    return item.id != dispatch.content
                })
            }

        case EMPRESA_ACTIONS.SALVAR:
            return {
                ...state,
                empresaLista: state.empresaLista.concat(dispatch.content),
                empresaItem: dispatch.content
            }

        case EMPRESA_ACTIONS.ALTERAR:
            return {
                ...state,
                empresaLista: state.empresaLista.map( empresa => {
                    if(empresa.id == dispatch.content.id){
                        return dispatch.content;
                    }
                    return empresa;
                }),
                empresaItem: dispatch.content
            }
        
        default:
            return state;


    }

}