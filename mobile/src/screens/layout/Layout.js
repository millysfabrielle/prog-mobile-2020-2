import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, Image  } from 'react-native';

class Layout extends React.Component{ 

    constructor(props, context){
        super(props, context);

    }

    render(){
        return (
            <View>
            
                <Text> Estou na tela 1</Text>

                <Button
                    title="Lista de pessoas"
                    onPress={ () => this.props.navigation.push('UserList') }
                />

                <Button
                    title="Lista de empresas"
                    onPress={ () => this.props.navigation.push('EmpresaList') }
                />

                <Button
                    title="Câmera"
                    onPress={ () => this.props.navigation.push('CameraComponent') }
                />

            </View>
        );
    }

}

export default Layout


const styles = StyleSheet.create({

});

/* export default function Tela1(props) {

    const {navigation} = props;

    const {name} = props.route.params != undefined ? props.route.params : '';

    navigation.setParams({tittle: "Detalhes do usuário 2"});

    return(
        <View>
            
            <Text> Estou na tela 1</Text>

            {props.route.params != undefined &&
                <Text>Parâmetro: {name}</Text>  
            }

            <Button
                title="Ir para tela 2"
                onPress={ () => navigation.navigate('Tela2', { name: 'Enviado pela TELA 1' }) }
            />

        </View>
    );

} */
