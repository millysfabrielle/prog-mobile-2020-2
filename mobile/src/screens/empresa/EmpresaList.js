import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, TextInput  } from 'react-native';

import { connect } from "react-redux";
import {listarEmpresa} from "../../redux/actions/EmpresaActions";
import {empresaListaSelector} from "../../redux/selectors/EmpresaSelectors";

////////
import EmpresaListComponent from "../../components/list/EmpresaListComponent";
///////

class EmpresaList extends React.Component{ 

    constructor(props, context){
        super(props, context);
        this.props.listarEmpresa(); // dispara a action
        this.state = {
            search: "",
        }
    }

    
    handleSearchChange(text){
        this.setState({search: text})
    }

    render(){
        return(
            <View>
                <Text>Lista de empresas</Text>
                
                <TextInput style={styles.textInput} onChangeText={ text => this.handleSearchChange(text) }/>


                <FlatList 
                    data={this.props.empresaLista.filter( empresa => empresa.nome.toLowerCase().includes(this.state.search.toLowerCase()))}
                    renderItem={EmpresaListComponent}
                    keyExtractor={ (empresa,index) => `empresa-list-${index}` }
                />

                <Button
                onPress={ () => this.props.navigation.push('EmpresaForm',{ id: undefined }) }
                title="Novo"
                color="#841584"
                />
                
            </View>
        );
    };


}

export default connect(empresaListaSelector, {listarEmpresa})(EmpresaList);


const styles = StyleSheet.create({
    textInput: {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1 
    }
});