import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, TextInput  } from 'react-native';

import { connect } from "react-redux";
import {buscarEmpresa, salvarEmpresa, alterarEmpresa} from "../../redux/actions/EmpresaActions";
import {empresaItemSelector, empresaListaSelector} from "../../redux/selectors/EmpresaSelectors";

import { Formik } from 'formik';

var initialValues= {
    nome: "",
    cnpj: "",
    endereco: ""

}

class EmpresaForm extends React.Component{ 

    constructor(props, context){
        super(props, context);

        if(this.props.route.params.id != undefined){
            this.props.buscarEmpresa(this.id);
        }
    }

    handleSubmit = (values, {setSubmitting}) => {

        setSubmitting(false);
        this.props.route.params.id ? this.props.alterarEmpresa(values) : this.props.salvarEmpresa(values);
        this.props.navigation.goBack();

    }

    render(){

        return(
            <View>
                <Text> Formulário de Empresa </Text>

                <Formik
                    initialValues= { this.props.route.params.id ? this.props.empresaItem : initialValues }
                    onSubmit={ (values, actions) => this.handleSubmit(values, actions)}
                >

                {({ handleChange, handleBlur, handleSubmit, isSubmitting, values }) => (
                    
                    <View>

                        <TextInput
                            placeholder="Nome"
                            onChangeText={handleChange('nome')}
                            onBlur={handleBlur('nome')}
                            value={values.nome}
                        />

                        <TextInput
                            placeholder="CNPJ"
                            onChangeText={handleChange('cnpj')}
                            onBlur={handleBlur('cnpj')}
                            value={values.cnpj}
                        />

                        <TextInput
                            placeholder="Endereço"
                            onChangeText={handleChange('endereco')}
                            onBlur={handleBlur('endereco')}
                            value={values.endereco}
                        />

                        <Button onPress={handleSubmit} title="Submit" disabled={isSubmitting} />

                    </View>

                )}


                </Formik>

            </View>
        );

    }

}


export default connect(empresaItemSelector,{buscarEmpresa, salvarEmpresa, alterarEmpresa})(EmpresaForm);