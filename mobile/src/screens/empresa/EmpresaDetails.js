import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, TextInput  } from 'react-native';

import { connect } from "react-redux";
import {buscarEmpresa, deletarEmpresa} from "../../redux/actions/EmpresaActions";
import {empresaItemSelector} from "../../redux/selectors/EmpresaSelectors";


class EmpresaDetails extends React.Component{ 

    constructor(props, context){
        super(props, context);

        // recuperar o parâmetro 
        const {id} = this.props.route.params;
        this.props.buscarEmpresa(id);

        this.props.navigation.setOptions({
            title: "Detalhes da empresa",
            headerRight: () => (
                <View>
                    <Button onPress={ () => this.props.navigation.navigate('EmpresaForm', { id: id }) } title="Alterar" />
                    <Button onPress={ () => this.handleDelete(id) } title="Deletar" />
                </View>
            ),
        });

    }

    handleDelete = (id) => {

        this.props.deletarEmpresa(id);
        this.props.navigation.goBack();
       
    }

    render(){

        return(
            <View>
                <Text> Nome = {this.props.empresaItem.nome} </Text>
                <Text> CNPJ = {this.props.empresaItem.cnpj} </Text>
                <Text> Endereço = {this.props.empresaItem.endereco} </Text>

            </View>
        );

    }

}

export default connect(empresaItemSelector, {buscarEmpresa, deletarEmpresa})(EmpresaDetails);

const styles = StyleSheet.create({});