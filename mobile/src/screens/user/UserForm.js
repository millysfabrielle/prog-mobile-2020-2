import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, TextInput  } from 'react-native';

import { connect } from "react-redux";
import {buscarPessoa, salvarPessoa, alterarPessoa} from "../../redux/actions/PessoaActions";
import {pessoaItemSelector} from "../../redux/selectors/PessoaSelectors";

import { Formik } from 'formik';

var initialValues= {
    nome: ""
}

class UserForm extends React.Component{ 

    constructor(props, context){
        super(props, context);

        if(this.props.route.params.id != undefined){
            this.props.buscarPessoa(this.id);
        }
    }

    handleSubmit = (values, {setSubmitting}) => {

        setSubmitting(false);
        this.props.route.params.id ? this.props.alterarPessoa(values) : this.props.salvarPessoa(values);
        this.props.navigation.goBack();

    }

    render(){

        return(
            <View>
                <Text> Formulário de Pessoa </Text>

                <Formik
                    initialValues= { this.props.route.params.id ? this.props.pessoaItem : initialValues }
                    onSubmit={ (values, actions) => this.handleSubmit(values, actions)}
                >

                {({ handleChange, handleBlur, handleSubmit, isSubmitting, values }) => (
                    
                    <View>

                        <TextInput
                            placeholder="Nome"
                            onChangeText={handleChange('nome')}
                            onBlur={handleBlur('nome')}
                            value={values.nome}
                        />

                        <Button onPress={handleSubmit} title="Submit" disabled={isSubmitting} />

                    </View>

                )}


                </Formik>

            </View>
        );

    }

}


export default connect(pessoaItemSelector,{buscarPessoa, salvarPessoa, alterarPessoa})(UserForm);