import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, TextInput  } from 'react-native';

import { connect } from "react-redux";
import {listarPessoa} from "../../redux/actions/PessoaActions";
import {pessoaListaSelector} from "../../redux/selectors/PessoaSelectors";

import UserListComponent from "../../components/list/UserListComponent";


class UserList extends React.Component{ 

    constructor(props, context){
        super(props, context);
        this.props.listarPessoa(); // dispara a action
        this.state = {
            search: "",
        }
    }

    
    handleSearchChange(text){
        this.setState({search: text})
    }

    render(){
        return(
            <View>
                <Text>Tela 2</Text>
                
                <TextInput style={styles.textInput} onChangeText={ text => this.handleSearchChange(text) }/>


                <FlatList 
                    data={this.props.pessoaLista.filter( pessoa => pessoa.nome.toLowerCase().includes(this.state.search.toLowerCase()))}
                    renderItem={UserListComponent}
                    keyExtractor={ (pessoa,index) => `user-list-${index}` }
                />

                <Button
                onPress={ () => this.props.navigation.push('UserForm',{ id: undefined }) }
                title="Novo"
                color="#841584"
                />
                
            </View>
        );
    };


}

export default connect(pessoaListaSelector, {listarPessoa})(UserList);


const styles = StyleSheet.create({
    textInput: {
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1 
    }
});