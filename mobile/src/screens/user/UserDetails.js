import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, TextInput  } from 'react-native';

import { connect } from "react-redux";
import {buscarPessoa, deletarPessoa} from "../../redux/actions/PessoaActions";
import {pessoaItemSelector} from "../../redux/selectors/PessoaSelectors";


class UserDetails extends React.Component{ 

    constructor(props, context){
        super(props, context);

        // recuperar o parâmetro 
        const {userID} = this.props.route.params;
        this.props.buscarPessoa(userID);

        this.props.navigation.setOptions({
            title: "Detalhes do usuário",
            headerRight: () => (
                <View>
                    <Button onPress={ () => this.props.navigation.navigate('UserForm', { id: userID }) } title="Alterar" />
                    <Button onPress={ () => this.handleDelete(userID) } title="Deletar" />
                </View>
            ),
        });

    }

    handleDelete = (id) => {

        this.props.deletarPessoa(id);
        this.props.navigation.goBack();
       
    }

    render(){

        return(
            <View>

                <Text> Pessoa = {this.props.pessoaItem.nome} </Text>

            </View>
        );

    }

}

export default connect(pessoaItemSelector, {buscarPessoa, deletarPessoa})(UserDetails);

const styles = StyleSheet.create({});