import React from 'react';
import { StyleSheet, Text, View, Button, FlatList, Image, TouchableOpacity  } from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons'; 

import * as MediaLibrary from 'expo-media-library';
import { Camera } from 'expo-camera';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';

class CameraComponent extends React.Component{ 

    constructor(props, context){
        super(props, context);

        this.state = {
            hasPermission: null,
            type: Camera.Constants.Type.back
        }

    }

    async componentDidMount(){
        this.getPermissionAsync()
    }

    getPermissionAsync = async () => {

         // Camera roll Permission 
        if (Platform.OS === 'ios') {
            const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
            if (status !== 'granted') {
              alert('Sorry, we need camera roll permissions to make this work!');
            }
        }

        const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
        this.setState( { hasPermission: status ==='granted'} );

    }


    handleCameraType = () => {
        const { cameraType } = this.state

        this.setState({
            cameraType:
                cameraType === Camera.Constants.Type.back
                    ? Camera.Constants.Type.front
                    : Camera.Constants.Type.back
        })
    }

    takePicture = async () => {
        if (this.camera) {
            let { uri } = await this.camera.takePictureAsync();
            const asset = await MediaLibrary.createAssetAsync(uri);
        }
    }

    pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images,
        });
    }


    render() {

        const { hasPermission } = this.state;

        if (hasPermission === null) {
            return (
                <View>
                    <Text> 404 !</Text>
                </View>
    
            );
        } else if (hasPermission === false) {
            return (
                <View>
                    <Text> You shall not pass!</Text>
                </View>
            );
        } else {
            return (
/*             <View style={{ flex: 1 }}>
                <Camera style={{ flex: 1 }} type={this.state.cameraType}></Camera>
            </View> */
            
            <View style={{ flex: 1 }}>
                <Camera style={{ flex: 1 }} type={this.state.cameraType}  ref={ref => {this.camera = ref}}>
                    <View style={{flex:1, flexDirection:"row",justifyContent:"space-between",margin:20}}>

                        <TouchableOpacity
                            style={{
                                alignSelf: 'flex-end',
                                alignItems: 'center',
                                backgroundColor: 'transparent',
                            }}
                            onPress={()=>this.pickImage()}
                        >
                            <Ionicons name="ios-photos" style={{ color: "#000", fontSize: 40 }} />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{
                                alignSelf: 'flex-end',
                                alignItems: 'center',
                                backgroundColor: 'transparent',
                            }}
                            onPress={()=>this.takePicture()} 
                        >
                            <Ionicons name="md-camera" style={{ color: "#000", fontSize: 40 }} />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={{
                                alignSelf: 'flex-end',
                                alignItems: 'center',
                                backgroundColor: 'transparent',
                            }}
                            onPress={()=>this.handleCameraType()}
                        >
                            <MaterialIcons name="switch-camera" size={40} color="black" />
                            
                        </TouchableOpacity>

                    </View>
                </Camera>
            </View>

            );
        }
        
    }


}

export default CameraComponent;