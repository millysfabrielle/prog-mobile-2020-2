import axios from 'axios'; // npm install axios

class EmpresaService{

    constructor(){
        this.connection = axios.create({baseURL: 'http://192.168.42.193:8000'});
    }

    listar(){
        return this.connection.get('/empresa');
    }

    buscar(id){
        return this.connection.get('/empresa/'+id);
    }

    salvar(empresa){
        if(empresa.id){
            return this.connection.put('/empresa', empresa);
        }
        return this.connection.post('/empresa', empresa);
    }

    excluir(id){
        return this.connection.delete('/empresa/'+id);
    }

}

export default new EmpresaService();