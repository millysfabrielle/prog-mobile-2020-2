import axios from 'axios'; // npm install axios

class PessoaService{

    constructor(){
        this.connection = axios.create({baseURL: 'http://192.168.42.193:8000'});
    }

    listar(){
        return this.connection.get('/pessoa');
    }

    buscar(id){
        return this.connection.get('/pessoa/'+id);
    }

    salvar(pessoa){
        if(pessoa.id){
            return this.connection.put('/pessoa', pessoa);
        }
        return this.connection.post('/pessoa', pessoa);
    }

    excluir(id){
        return this.connection.delete('/pessoa/'+id);
    }

}

export default new PessoaService();