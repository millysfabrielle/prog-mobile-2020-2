import React from 'react';

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, FlatList, Image  } from 'react-native';

import {Provider} from "react-redux";
import store from "./src/redux/Store";

import Layout from './src/screens/layout/Layout';

import UserList from './src/screens/user/UserList';
import UserDetails from "./src/screens/user/UserDetails";
import UserForm from "./src/screens/user/UserForm";

import EmpresaList from "./src/screens/empresa/EmpresaList";
import EmpresaDetails from "./src/screens/empresa/EmpresaDetails";
import EmpresaForm from "./src/screens/empresa/EmpresaForm";

import CameraComponent from "./src/screens/camera/CameraComponent";



const Stack = createStackNavigator();


export default function App() {


  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Layout">
          <Stack.Screen name="Layout" component={Layout} />

          <Stack.Screen name="UserList" component={UserList} />
          <Stack.Screen name="UserDetails" component={UserDetails}/>
          <Stack.Screen name="UserForm" component={UserForm}/>

          <Stack.Screen name="EmpresaList" component={EmpresaList}/>
          <Stack.Screen name="EmpresaDetails" component={EmpresaDetails}/>
          <Stack.Screen name="EmpresaForm" component={EmpresaForm}/>

          <Stack.Screen name="CameraComponent" component={CameraComponent} />

        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

const styles = StyleSheet.create({
  btnDelete:{
    width: 20,
    height: 20,
    color: "#444"
  },
  nav:{
    marginTop: 50,
    backgroundColor: '#ff0000'
  },
  topo:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#0f3460',
    width: "100%",
    marginBottom: 30
  },
  logo: {
    width: 66,
    height: 58,
  }   
});
