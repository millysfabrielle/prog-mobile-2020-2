import React from 'react';

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, FlatList, Image  } from 'react-native';

import Tela1 from './screens/Tela1';
import Tela2 from './screens/Tela2';

const Stack = createStackNavigator();

export default function App() {

  return (
    <NavigationContainer>
       <Stack.Navigator initialRouteName='Tela1'>
        <Stack.Screen name="Tela1" component={Tela1} options={{ title: 'Tela 1' }} />
        <Stack.Screen name="Tela2" component={Tela2} options={{ title: 'Tela 2' }} />
       </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({

});
