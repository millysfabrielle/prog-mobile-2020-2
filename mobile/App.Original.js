import React from 'react';

import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';

import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Button, FlatList, Image  } from 'react-native';

const DATA = [
  {
    id: '1',
    name: 'Civic',
    description: '2018, ar, pneu furado',
    price: '89.000,00',
    thumb: "https://media.ed.edmunds-media.com/honda/civic/2020/oem/2020_honda_civic_coupe_si_fq_oem_1_815.jpg"
  },
  {
    id: '2',
    name: 'Accord',
    description: '2018, ar, pneu furado',
    price: '189.000,00',
    thumb: "https://images.noticiasautomotivas.com.br/img/f/honda-accord-2016-1-1200x900.jpeg"
  }
];


(Parâmetros) => (Algoritmo)

const Item = ({ id, name, description, price, thumb }) => (
  <View>
    <Image
        style={styles.logo}
        source={{uri: thumb}}
      />
    <Text>{name}</Text>
    <Text>{description}</Text>
    <Text>R$ {price}</Text>
  </View>
);


export default function App() {

  const renderItem = ({item}) => (
    <Item thumb={item.thumb} name={item.name} description={item.description} price={item.price}  ></Item>
  );


  return (
    <View>

      <StatusBar style="auto" />

      <View style={styles.topo}>  
        <Image
          style={styles.logo}
          source={require('./assets/img/menu-icon.png')}
        />
        <Image
          style={styles.logo}
          source={require('./assets/img/round-user.png')}
        />
      </View>

      <Button title="Comprar" onPress={() => Alert.alert('Simple Button pressed')} />

      <Button title="vender" onPress={() => Alert.alert('Simple Button pressed')} />

      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />

    </View>
  );
}

const styles = StyleSheet.create({
  topo:{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#0f3460',
    width: "100%"
  },
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    width: 66,
    height: 58,
  }
});
